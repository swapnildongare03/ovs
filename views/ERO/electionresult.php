<?php
require_once "../../Middleware/AuthEro.php";
require_once "../../Models/Auth.php";
require_once "../../Models/Ero.php";

$ero = $_SESSION['userEro'];
$eroObj = new Ero();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../../assets/images/logo.png">
    <title>OVS | ERO:Election results</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../bootstrap/js/jquery.slim.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>

    <link rel="stylesheet" href="/OVS/views/ERO/dashboard.css">

</head>

<body>



    <!-- dashboard -->
    <div class="container-fluid">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 text-center mainSidebar">
                <div class="row">
                    <nav class="navbar bg-warning sticky-top col-12" style="padding:10px;width:100%">
                        <img src="../../assets/images/4.jpg" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                        <a class="navbar-brand ml-2 text-white" href="/OVS">Dashboard</a>
                        <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/dashboard.php">Home</a>
                    </div>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/voterlist.php">Voter</a>
                    </div>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/elections.php">Election</a>
                    </div>

                    <div class="col-12 sidebarList sidebarActive">
                        <a href="/OVS/views/ERO/electionresult.php"> Election Results</a>
                    </div>
                </div>
            </div>
            <!-- sidebar end -->
            <!--  -->
            <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-warning col-12" style="padding:16px;">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            Welcome &nbsp;
                            <strong>
                                <?php
                                // var_dump($_SESSION['userEro']);                 
                                echo $ero['username'];
                                ?> </strong>
                        </ul>
                        <div style="float:right">
                            <a href="../../Models/Logout.php?logout" class="btn btn-outline-info">Logout</a>

                        </div>
                    </div>
                </nav>

                <!-- End Navigation -->

                <!-- Main Content -->
                <div class="container-fluid mainContent" style="background-color: #f2f2f2;">

                    <div class="row mt-4">
                        <div class="col-2"></div>
                        <div class="col-8">
                            <?php

                            if (!empty($_SESSION['error'])) {
                            ?>
                                <div class="row error-alert">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <div>
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <strong>
                                                    <?php
                                                    echo $_SESSION['error'];
                                                    ?>
                                                </strong>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                unset($_SESSION['error']);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <h3 class="text-secondary">Election Results</h3>
                        </div>
                    </div>

                    <?php
                    $data = $eroObj->getOngoingElections(); //get the value of ongoing Election
                    ?>

                    <div class="container">
                        <div class="row">
                            <div class="col-md-11 card p-0">
                                <div class="card-header bg-primary text-white">
                                    Ongoing Elections
                                </div>
                                <div class="card-body">
                                    <table class="table table-light">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>Election Id </th>
                                                <th>Election Area</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if ($data) {
                                                $count = 1;
                                                foreach ($data as $election) {
                                            ?>

                                                    <tr>
                                                        <td><?php echo $count; ?></td>
                                                        <td><?php echo $election['election_id']; ?></td>
                                                        <td><?php echo $election['election_area']; ?></td>
                                                        <td>
                                                            <a href="/OVS/views/ERO/electiondetails.php?eleID=<?php echo $election['election_id']; ?>" class="btn btn-info">View More -></a>
                                                        </td>
                                                    </tr>

                                            <?php
                                                    $count++;
                                                }
                                            } else {
                                                echo '
                                                    <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>No Data Found</td>
                                                    <td>

                                                    </td>
                                                </tr>
                                                    ';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-footer ">
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    $data = $eroObj->getUpcomingElections(); //get the value of upcoming Election
                    ?>

                    <div class="container mt-4">
                        <div class="row">
                            <div class="col-md-11 card p-0">
                                <div class="card-header bg-success text-white">
                                    Upcoming Elections
                                </div>
                                <div class="card-body">
                                    <table class="table table-light">
                                        <thead class="thead-light">
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>Election Id </th>
                                                <th>Election Area</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        if ($data) {
                                            $count = 1;
                                            foreach ($data as $election) {
                                        ?>

                                                <tr>
                                                    <td><?php echo $count; ?></td>
                                                    <td><?php echo $election['election_id']; ?></td>
                                                    <td><?php echo $election['election_area']; ?></td>
                                                    <td>
                                                        <a href="/OVS/views/ERO/electiondetails.php?eleID=<?php echo $election['election_id']; ?>" class="btn btn-info">View More -></a>
                                                    </td>
                                                </tr>

                                        <?php
                                                $count++;
                                            }
                                        } else {
                                            echo '
                                                    <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>No Data Found</td>
                                                    <td>

                                                    </td>
                                                </tr>
                                                    ';
                                        }
                                        ?>
                                    </table>
                                </div>
                                <div class="card-footer ">
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    $data = $eroObj->getDoneElection(); //get the value of done Election
                    ?>

                    <div class="container mt-4">
                        <div class="row">
                            <div class="col-md-11 card p-0">
                                <div class="card-header bg-warning text-white">
                                    Done Elections
                                </div>
                                <div class="card-body">
                                    <table class="table table-light">
                                        <thead class="thead-light">
                                            <tr class="bg-light">
                                                <th>Sr.No</th>
                                                <th>Election Id </th>
                                                <th>Election Area</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        if ($data) {
                                            $count = 1;
                                            foreach ($data as $election) {
                                        ?>

                                                <tr>
                                                    <td><?php echo $count; ?></td>
                                                    <td><?php echo $election['election_id']; ?></td>
                                                    <td><?php echo $election['election_area']; ?></td>
                                                    <td>
                                                        <a href="/OVS/views/ERO/electiondetails.php?eleID=<?php echo $election['election_id']; ?>" class="btn btn-info">View More -></a>
                                                        <?php
                                                        if (!$eroObj->isResultGenerated($election['election_id'])) {
                                                            echo '<a href="#" class="btn btn-outline-success">Generate Result</a>';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>

                                        <?php
                                                $count++;
                                            }
                                        } else {
                                            echo '
                                                    <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td>No Data Found</td>
                                                    <td>

                                                    </td>
                                                </tr>
                                                    ';
                                        }
                                        ?>
                                    </table>
                                </div>
                                <div class="card-footer ">
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
                <!-- End Main Content -->
            </div>

        </div>
    </div>


</body>

</html>