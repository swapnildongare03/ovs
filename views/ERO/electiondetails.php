<?php
include_once '../../Models/Ero.php';
include_once '../../Models/DB.php';
require_once "../../Middleware/AuthEro.php";
$ero = $_SESSION['userEro'];
$eroObj = new Ero();

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../../assets/images/logo.png">
    <title>OVS | ERO:Dashboard</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>

    <link rel="stylesheet" href="/OVS/views/ERO/dashboard.css">
</head>

<body>
    <!-- dashboard -->
    <div class="container-fluid">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 text-center mainSidebar">
                <div class="row">
                    <nav class="navbar bg-warning sticky-top col-12" style="padding:10px;width:100%;">
                        <img src="../../assets/images/4.jpg" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                        <a class="navbar-brand ml-2 text-white" href="/OVS">Dashboard</a>
                        <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/dashboard.php">Home</a>
                    </div>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/voterlist.php">Voter</a>
                    </div>
                    <div class="col-12 sidebarList sidebarActive">
                        <a href="/OVS/views/ERO/elections.php">Election</a>
                    </div>

                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/electionresult.php"> Election Results</a>
                    </div>
                </div>
            </div>
            <!-- sidebar end -->
            <!--  -->
            <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-warning col-12" style="padding:16px;">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            Welcome &nbsp;
                            <strong>
                                <?php
                                // var_dump($_SESSION['userEro']);                 
                                echo $ero['username'];
                                ?> </strong>
                        </ul>
                        <div style="float:right">
                            <a href="../../Models/Logout.php?logout" class="btn btn-outline-primary bg-white">Logout</a>

                        </div>
                    </div>
                </nav>

                <!-- End Navigation -->

                <!-- Main Content -->
                <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                    <div class="row mt-4">
                        <div class="col-12">
                            <?php

                            if (!empty($_SESSION['error'])) {
                            ?>
                                <div class="row error-alert">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <div>
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <strong>
                                                    <?php
                                                    echo $_SESSION['error'];
                                                    ?>
                                                </strong>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                unset($_SESSION['error']);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <?php
                        $data = Ero::getElectionRecord($_GET['eleID']);
                        ?>
                        <div class="col-1"></div>
                        <div class="col-10 card p-0 m-0">
                            <div class="card-header" style="display: inline-flex;">
                                <h3>Election details</h3>
                                <?php
                                if ($eroObj->isResultGenerated($_GET['eleID'])) {
                                    echo NULL;
                                } else {
                                    echo '
                                        <form method="POST" action="">
                                        <input type="hidden" name="eleID" value="' . $_GET['eleID'] . '">
                                        <button class="btn btn-success" style="margin-left:450px;" name="genRes">Geneate Result</button>
                                        </form>
                                        ';
                                }

                                ?>
                            </div>
                            <div class="card-body text-center">
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary"> Election Id</div>
                                    <div class="col-md-6">
                                        <?php echo $data['election_id']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary"> Election area</div>
                                    <div class="col-md-6">
                                        <?php echo $data['election_area']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary"> Election title</div>
                                    <div class="col-md-6">
                                        <?php echo $data['election_title']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary"> Election type</div>
                                    <div class="col-md-6">
                                        <?php echo $data['election_type']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary"> Election title</div>
                                    <div class="col-md-6">
                                        <?php echo $data['election_title']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary"> Election status</div>
                                    <div class="col-md-6">
                                        <?php
                                        if ($data['election_title']) {
                                            echo '<span class="text-success">Active</span>';
                                        } else {
                                            echo '<span class="text-danger">Inactive</span>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary">Registration start date</div>
                                    <div class="col-md-6">
                                        <?php echo $data['reg_start_date']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary">Registration end date</div>
                                    <div class="col-md-6">
                                        <?php echo $data['reg_end_date']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary">Election start date and time</div>
                                    <div class="col-md-6">
                                        <?php echo $data['election_start_date']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1">
                                    <div class="col-md-6 text-secondary">Election end date and time</div>
                                    <div class="col-md-6">
                                        <?php echo $data['election_end_date']; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="container-fluid mt-3">
                        <div class="row">
                            <div class="col-12 card p-0 m-0">
                                <div class="card-header">
                                    <h3 class="text-secondary">Registered Candidate for election</h3>
                                </div>
                                <div class="card-body">
                                    <table class="table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">Sr. No</th>
                                                <th scope="col">candidate Id</th>
                                                <th scope="col">voter Id</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <?php
                                        $count = 1;
                                        $candidate = Ero::getElectionCandidateRecord($_GET['eleID']);

                                        ?>
                                        <tbody id="voterTable">
                                            <?php
                                            if (!empty($candidate)) {
                                                foreach ($candidate as $candi) {


                                            ?>
                                                    <tr class="
                                                <?php
                                                    if ($candi['is_winner']) {
                                                        echo "bg-success";
                                                    }
                                                ?>
                                        ">
                                                        <th scope="row"><?php echo $count; ?></th>
                                                        <th scope="row"><?php echo $candi['candidate_id']; ?></th>
                                                        <td><?php echo $candi['voter_id']; ?></td>
                                                        <td>
                                                            <a href="/OVS/views/ERO/candidatedetails.php?candiID=<?php echo $candi['candidate_id']; ?>" class="btn btn-primary">See more</a>
                                                        </td>

                                                    </tr>

                                            <?php
                                                    $count++;
                                                }
                                            } else {
                                                echo ('
                                        <tr>
                                        <th scope="row"></th>
                                        <td>no data found</td>
                                        <td></td>
                                        <td></td>
                                        </tr>
                                        ');
                                            }

                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Main Content -->
            </div>

        </div>
    </div>



</body>

</html>

<?php

if (isset($_POST['genRes'])) {
    $eroObj->generateResult($_POST['eleID']);
}
?>