<?php
include_once '../../Models/Ero.php';
include_once '../../Models/DB.php';
require_once "../../Middleware/AuthEro.php";
$eroUser = $_SESSION['userEro'];

$ero = new Ero();
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../../assets/images/logo.png">
    <title>OVS | ERO:Dashboard</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>

    <link rel="stylesheet" href="/OVS/views/ERO/dashboard.css">

</head>

<body>



    <!-- dashboard -->
    <div class="container-fluid">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 text-center mainSidebar">
                <div class="row">
                    <nav class="navbar bg-warning sticky-top col-12" style="padding:10px;width:100%">
                        <img src="../../assets/images/4.jpg" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                        <a class="navbar-brand ml-2 text-white" href="/OVS">Dashboard</a>
                        <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/dashboard.php">Home</a>
                    </div>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/voterlist.php">Voter</a>
                    </div>
                    <div class="col-12 sidebarList sidebarActive">
                        <a href="/OVS/views/ERO/elections.php">Election</a>
                    </div>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/electionresult.php"> Election Results</a>
                    </div>
                </div>
            </div>
            <!-- sidebar end -->
            <!--  -->
            <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-warning col-12" style="padding:16px;">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            Welcome &nbsp;
                            <strong>
                                <?php
                                // var_dump($_SESSION['userEro']);                 
                                echo $eroUser['username'];
                                ?> </strong>
                        </ul>
                        <div style="float:right">
                            <a href="../../Models/Logout.php?logout" class="btn btn-outline-primary bg-white">Logout</a>

                        </div>
                    </div>
                </nav>

                <!-- End Navigation -->

                <!-- Main Content -->
                <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                    <div class="row mt-4">
                        <div class="col-12">
                            <?php

                            if (!empty($_SESSION['error'])) {
                            ?>
                                <div class="row error-alert">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <div>
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <strong>
                                                    <?php
                                                    echo $_SESSION['error'];
                                                    ?>
                                                </strong>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                unset($_SESSION['error']);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12">
                            <div class="float-right p-1" style="display: inline-flex;justify-content:space-between">
                                <form action="" method="post" class="m-1">
                                    <button class="btn btn-warning" name="resetAllElection">Reset all elections</button>
                                </form>
                                <form action="" method="post" class="m-1">
                                    <button class="btn btn-success" name="createElectionResult">Declare all elections result</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="card">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-6 form-group">
                                            <input type="search" name="" id="serchInput" class="form-control" placeholder=" Area pin to search ...">
                                        </div>
                                        <div class="col-6">
                                            <div style="float:right">
                                                <a href="/OVS/views/ERO/addElection.php" class="btn btn-outline-success">Create New Election</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">Sr. No</th>
                                                <th scope="col">Election Id</th>
                                                <th scope="col">Area</th>
                                                <th scope="col">Election details</th>
                                                <th scope="col">Status</th>
                                                <th scope="col"></th>

                                            </tr>
                                        </thead>
                                        <?php
                                        $count = 1;
                                        $row = $ero->getAllElectionRecords();
                                        ?>
                                        <tbody id="electionTable">
                                            <?php
                                            if (!empty($row)) {
                                                foreach ($row as $election) {


                                            ?>
                                                    <tr>
                                                        <th scope="row"><?php echo $count; ?></th>
                                                        <td><?php echo $election['election_id']; ?></td>
                                                        <td id="electionArea"><?php echo $election['election_area']; ?></td>
                                                        <td>
                                                            <!-- Large modal -->
                                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#<?php echo 'xyz' . $election['election_id'] . 'abs' ?>">Details</button>

                                                            <div class="modal fade bd-example-modal-lg" id="<?php echo 'xyz' . $election['election_id'] . 'abs' ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header text-center">
                                                                            <h3><strong>Election details</strong></h3>
                                                                        </div>

                                                                        <div class="modal-body">
                                                                            <div class="container-fluid  text-center">
                                                                                <div class="row mt-2 bg-light p-1 border-rounded">
                                                                                    <div class="col-6"><strong>Election Id :</strong></div>
                                                                                    <div class="col-6"><?php echo $election['election_id']; ?></div>
                                                                                </div>
                                                                                <div class="row mt-2 bg-light p-1 border-rounded">
                                                                                    <div class="col-6"><strong>Election title :</strong></div>
                                                                                    <div class="col-6"><?php echo $election['election_title']; ?></div>
                                                                                </div>

                                                                                <div class="row mt-2 bg-light p-1 border-rounded">
                                                                                    <div class="col-6"><strong>Election type :</strong></div>
                                                                                    <div class="col-6"><?php echo $election['election_type']; ?></div>
                                                                                </div>
                                                                                <div class="row mt-2 bg-light p-1 border-rounded">
                                                                                    <div class="col-6"><strong>area :</strong></div>
                                                                                    <div class="col-6"><?php echo $election['election_area']; ?></div>
                                                                                </div>
                                                                                <div class="row mt-2 bg-light p-1 border-rounded">
                                                                                    <div class="col-6"><strong>Status :</strong></div>
                                                                                    <div class="col-6">
                                                                                        <?php
                                                                                        if ($election['status']) {
                                                                                            echo "<span class='text-success'>Active</span>";
                                                                                        } else {
                                                                                            echo "<span class='text-danger'>Inactive</span>";
                                                                                        }
                                                                                        ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row mt-2 bg-light p-1 border-rounded ">
                                                                                    <div class="col-6"><strong>Registration start Date :</strong></div>
                                                                                    <div class="col-6"><?php echo $election['reg_start_date']; ?></div>
                                                                                </div>
                                                                                <div class="row mt-2 bg-light p-1 border-rounded">
                                                                                    <div class="col-6"><strong>Registration end date :</strong></div>
                                                                                    <div class="col-6"><?php echo $election['reg_end_date']; ?></div>
                                                                                </div>
                                                                                <div class="row mt-2 bg-light p-1 border-rounded">
                                                                                    <div class="col-6"><strong>Start Date :</strong></div>
                                                                                    <div class="col-6"><?php echo $election['election_start_date']; ?></div>
                                                                                </div>
                                                                                <div class="row mt-2 bg-light p-1 border-rounded">
                                                                                    <div class="col-6"><strong>End Date :</strong></div>
                                                                                    <div class="col-6"><?php echo $election['election_end_date']; ?></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                            <a href="/OVS/views/ERO/updateelection.php?eleID=<?php echo $election['election_id']; ?>" class="btn btn-warning">Update</a>
                                                                            <a href="/OVS/views/ERO/electiondetails.php?eleID=<?php echo  $election['election_id']; ?> " class="btn btn-info">See more -></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <?php
                                                            if ($election['status']) {
                                                                echo   '<a href="/OVS/views/ERO/elections.php?xyzId=' . $election["election_id"] . '" class="btn btn-danger">Make Inactive</a>';
                                                            } else {
                                                                echo   '<a href="/OVS/views/ERO/elections.php?xyzId=' . $election["election_id"] . '" class="btn btn-success">Make  Active</a>';
                                                            }

                                                            ?>
                                                        </td>
                                                    </tr>

                                            <?php
                                                    $count++;
                                                }
                                            } else {
                                                echo ('
                                        <tr>
                                        <th scope="row"></th>
                                        <td>no data found</td>
                                        <td></td>
                                        <td></td>
                                        </tr>
                                        ');
                                            }

                                            ?>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Main Content -->
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#serchInput').on('keyup', function() {
                var inputValue = $(this).val().toLowerCase();
                $('#electionTable tr').filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(inputValue) > -1);
                });
            });
        });
    </script>

</body>

</html>

<?php

if (isset($_GET['xyzId'])) {
    $ero = new Ero();
    echo  $ero->ElectionStatusManage($_GET['xyzId']);
}

if (isset($_POST['resetAllElection'])) {
    $ero->makeInactiveAllOutdatedElection();
}
?>