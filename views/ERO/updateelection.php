<?php
include_once '../../Models/Ero.php';
include_once '../../Models/DB.php';
require_once "../../Middleware/AuthEro.php";
$ero = $_SESSION['userEro'];
$eroObj = new Ero();

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../../assets/images/logo.png">
    <title>OVS | ERO:Dashboard</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>

    <link rel="stylesheet" href="/OVS/views/ERO/dashboard.css">
</head>

<body>



    <!-- dashboard -->
    <div class="container-fluid">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 text-center mainSidebar">
                <div class="row">
                    <nav class="navbar bg-warning sticky-top col-12" style="padding:10px;width:100%;">
                        <img src="../../assets/images/4.jpg" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                        <a class="navbar-brand ml-2 text-white" href="/OVS">Dashboard</a>
                        <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/dashboard.php">Home</a>
                    </div>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/voterlist.php">Voter</a>
                    </div>
                    <div class="col-12 sidebarList sidebarActive">
                        <a href="/OVS/views/ERO/elections.php">Election</a>
                    </div>

                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/electionresult.php"> Election Results</a>
                    </div>
                </div>
            </div>
            <!-- sidebar end -->
            <!--  -->
            <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-warning col-12" style="padding:16px;">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            Welcome &nbsp;
                            <strong>
                                <?php
                                // var_dump($_SESSION['userEro']);                 
                                echo $ero['username'];
                                ?> </strong>
                        </ul>
                        <div style="float:right">
                            <a href="../../Models/Logout.php?logout" class="btn btn-outline-primary bg-white">Logout</a>

                        </div>
                    </div>
                </nav>

                <!-- End Navigation -->

                <!-- Main Content -->
                <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                    <div class="row mt-4">
                        <div class="col-12">
                            <?php

                            if (!empty($_SESSION['error'])) {
                            ?>
                                <div class="row error-alert">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <div>
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                <strong>
                                                    <?php
                                                    echo $_SESSION['error'];
                                                    ?>
                                                </strong>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                unset($_SESSION['error']);
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <?php
                        $data = Ero::getElectionRecord($_GET['eleID']);
                        ?>
                        <div class="col-1"></div>
                        <div class="col-10 card p-0 m-0">
                            <div class="card-header" style="display: inline-flex;">
                                <h5>Update Election details</h5>
                            </div>
                            <div class="card-body text-center">

                                <?php
                                if ($eroObj->isResultGenerated($_GET['eleID'])) {
                                    echo '
                                    <div class="row bg-light p-1 mt-1">
                                        <div class="col-md-12 text-center text-danger">
                                            You can not update this election . Result is already declared for this election !
                                        </div>
                                    </div>
                                </div>
                                    ';
                                } else {
                                ?>
                                    <form action="" method="post">
                                        <div class="row">
                                            <div class="col-6 p-2" hidden>
                                                <div class="form-group">
                                                    <label for="electionid">Election id</label>
                                                    <input id="electionid" class="form-control" type="text" name="election_id" value="<?php echo $data['election_id']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-6 p-2">
                                                <div class="form-group">
                                                    <label for="electiontitle">Title</label>
                                                    <input id="electiontitle" class="form-control" type="text" name="election_title" value="<?php echo $data['election_title']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-6 p-2">
                                                <div class="form-group">
                                                    <label for="electiontype">Type</label>
                                                    <input id="electiontype" class="form-control" type="text" name="election_type" value="<?php echo $data['election_type']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-6 p-2">
                                                <div class="form-group">
                                                    <label for="electionarea">Area</label>
                                                    <input id="electionarea" class="form-control" type="text" name="election_area" value="<?php echo $data['election_area']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-6 p-2">
                                                <div class="form-group mt-3" style="display: flex;">
                                                    <label for="electionstatus">Status</label>
                                                    <input id="electionstatus" class="form-control text-decoration-none" type="radio" name="election_status" value="1" <?php if ($data['status'] == 1) {
                                                                                                                                                                            echo 'checked';
                                                                                                                                                                        } ?>>Active
                                                    <input id="electionstatus" class="form-control" type="radio" name="election_status" value="0" <?php if ($data['status'] == 0) {
                                                                                                                                                        echo 'checked';
                                                                                                                                                    } ?>>Inactive
                                                </div>
                                            </div>
                                            <div class="col-6 p-2">
                                                <div class="form-group">
                                                    <label for="electionregst">Registration start Date</label>
                                                    <input id="electionregst" class="form-control" type="date" name="reg_start_date" value="<?php echo $data['reg_start_date']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-6 p-2">
                                                <div class="form-group">
                                                    <label for="electionregend">Registration End Date</label>
                                                    <input id="electionregend" class="form-control" type="date" name="reg_end_date" value="<?php echo $data['reg_end_date']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-6 p-2">
                                                <div class="form-group">
                                                    <label for="electionelest">Election Start Date <small class="text-secondary"><?php echo $data['election_start_date']; ?></small></label>
                                                    <input id="electionelest" class="form-control" type="datetime-local" name="election_start_date" value="<?php echo $data['election_start_date']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-6 p-2">
                                                <div class="form-group">
                                                    <label for="electioneleend">Election End Date <small class="text-secondary"><?php echo $data['election_end_date']; ?></small></label>
                                                    <input id="electioneleend" class="form-control" type="datetime-local" name="election_end_date" value="<?php echo $data['election_end_date']; ?>">
                                                </div>
                                            </div>
                                            <div class="col-12 text-center">
                                                <button class="btn btn-info form-control" name="btnUpdateEle">Update</button>
                                            </div>
                                        </div>
                                    </form>

                                <?php
                                } //else of result generated ends here 
                                ?>
                            </div>
                        </div>
                    </div>


                    <?php
                    if (isset($_POST['btnUpdateEle'])) {
                        $election_id = $_POST['election_id'];
                        $election_title = $_POST['election_title'];
                        $election_type = $_POST['election_type'];
                        $election_area = $_POST['election_area'];
                        $election_status = $_POST['election_status'];
                        $reg_start_date = $_POST['reg_start_date'];
                        $reg_end_date = $_POST['reg_end_date'];
                        $election_start_date = $_POST['election_start_date'];
                        $election_end_date = $_POST['election_end_date'];

                        # echo "$election_id  ... $election_title ... $election_type ... $election_area ... $election_status ... $reg_start_date ... $reg_end_date ... $election_start_date ... $election_end_date";
                        if (!empty($election_id) && !empty($election_title) && !empty($election_type) && !empty($election_area)  && !empty($reg_start_date) && !empty($reg_end_date) && !empty($election_start_date) && !empty($election_end_date)) {
                            $eroObj->updateElection($election_id, $election_title, $election_type, $election_area, $election_status, $reg_start_date, $reg_end_date, $election_start_date, $election_end_date);
                        } else {
                            echo '<script>
                                alert("all fields area required !");
                                window.history.back();
                            </script>';
                        }
                    }
                    ?>

                </div>
                <!-- End Main Content -->
            </div>

        </div>
    </div>



</body>

</html>