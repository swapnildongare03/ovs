<?php
include_once '../../Models/Ero.php';
include_once '../../Models/DB.php';
require_once "../../Middleware/AuthEro.php";

$ero = $_SESSION['userEro'];
$eroObj = new Ero();
if (!isset($_GET['candiID'])) {
    header('location:/OVS/views/ERO/elections.php');
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../../assets/images/logo.png">
    <title>OVS | ERO:Dashboard</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../bootstrap/js/jquery.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>

    <link rel="stylesheet" href="/OVS/views/ERO/dashboard.css">

</head>

<body>



    <!-- dashboard -->
    <div class="container-fluid">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 text-center mainSidebar">
                <div class="row">
                    <nav class="navbar bg-warning sticky-top col-12" style="padding:10px;width:100%">
                        <img src="../../assets/images/4.jpg" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                        <a class="navbar-brand ml-2 text-white" href="/OVS">Dashboard</a>
                        <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/dashboard.php">Home</a>
                    </div>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/voterlist.php">Voter</a>
                    </div>
                    <div class="col-12 sidebarList sidebarActive">
                        <a href="/OVS/views/ERO/elections.php">Election</a>
                    </div>

                    <<div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/electionresult.php"> Election Results</a>
                </div>
            </div>
        </div>
        <!-- sidebar end -->
        <!--  -->
        <div class="col-md-9" style="padding:0px;margin:0px">

            <!-- Start Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-warning col-12" style="padding:16px;">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        Welcome &nbsp;
                        <strong>
                            <?php
                            // var_dump($_SESSION['userEro']);                 
                            echo $ero['username'];
                            ?> </strong>
                    </ul>
                    <div style="float:right">
                        <a href="../../Models/Logout.php?logout" class="btn btn-outline-primary bg-white">Logout</a>

                    </div>
                </div>
            </nav>

            <!-- End Navigation -->

            <?php
            $data = Ero::getCandidateRecord($_GET['candiID']);
            ?>

            <!-- Main Content -->
            <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                <div class="row mt-4">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <?php

                        if (!empty($_SESSION['error'])) {
                        ?>
                            <div class="row error-alert">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <strong>
                                                <?php
                                                echo $_SESSION['error'];
                                                ?>
                                            </strong>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                            unset($_SESSION['error']);
                        }
                        ?>
                    </div>
                </div>
                <div class="row mt-3 mb-4">
                    <div class="col-1"></div>
                    <div class="col-10 card p-0 m-0">
                        <div class="card-header">
                            <h3 class="text-secondary">Candidate details</h3>
                        </div>
                        <div class="card-body">
                            <?php
                            if ($data == null) {
                                echo '<div class="text-center text-secondary>No data found</div>"';
                            } else {
                            ?>
                                <div class="row" style="align-items: center;align-self:center;text-align:center">
                                    <div class="col-4 ">
                                        <?php
                                        if ($eroObj->isResultGenerated($data['election_id'])) {
                                            if ($data['is_winner']) {
                                                echo '<span class="bg-success text-white p-4">Winner</span>';
                                            } else {
                                                echo '<span class="bg-danger text-white p-4">loss </span>';
                                            }
                                        }

                                        ?>
                                    </div>
                                    <div class="col-4 text-center">
                                        <?php if ($data['profile'] != NULL) { ?>
                                            <img src="<?php echo $data['profile']; ?>" alt="" class="img-fluid img-thumbnail">
                                        <?php } else {
                                            echo '<img class="img-fluid img-thumbnail" src="/OVS/assets/images/logo.png">';
                                        }
                                        ?>
                                    </div>
                                    <div class="col-4">
                                        <?php
                                        if ($eroObj->isResultGenerated($data['election_id'])) {
                                            echo "<span class='text-secondary'>Vote  :    " . $data['vote_count'] . "</span>";
                                        }

                                        ?>
                                    </div>
                                </div>


                                <?php
                                if ($data['candidate_status'] != null) {
                                    if ($data['candidate_status']) {
                                        echo '
                                                    <div class="row bg-success text-center text-white">
                                                        <div class="col-12">
                                                                <h5>Application Accepted</h5>
                                                        </div>
                                                    </div>
                                                ';
                                    } else {
                                        echo '
                                                <div class="row bg-danger text-center text-white">
                                                    <div class="col-12">
                                                            <h5>Application Rejected</h5>
                                                    </div>
                                                </div>
                                            ';
                                    }
                                }
                                ?>

                                <div class="row bg-light p-1 mt-3 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Candidate Id
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $data['candidate_id']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-3 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Voter Id
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $data['voter_id']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Name
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $data['voter_name']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Date of Birth
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        $date = date("d/m/Y", strtotime($data['dob']));
                                        echo $date; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Age
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        $bday = new DateTime($data['dob']);
                                        $today = new Datetime(date('y.m.d'));
                                        $diff = $today->diff($bday);
                                        $age = $diff->y;
                                        echo $age; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Gender
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        echo ucfirst($data['gender']); ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Area
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        echo $data['area']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Address
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        echo $data['address']; ?>
                                    </div>
                                </div>

                                <div class="row mt-2 font-weight-bold">
                                    <div class="col-12 p-3 text-warning">
                                        <h5>Election details</h5>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Election id
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        echo $data['election_id']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Election title
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        echo $data['election_title']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Election type
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        echo $data['election_type']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Election area
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        echo $data['election_area']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Election status
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        if ($data['election_status']) {

                                            echo '<span class="bg-success text-white p-2">Active</span>';
                                        } else {
                                            echo '<span class="bg-danger text-white p-2">Ina    ctive</span>';
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Election registration start date
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        $date = date("d/m/Y", strtotime($data['election_reg_start_date']));
                                        echo $date; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Election registration end date
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        $date = date("d/m/Y", strtotime($data['election_reg_end_date']));
                                        echo $date; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Election start date
                                    </div>
                                    <div class="col-md-6">
                                        <?php
                                        echo $data['election_start_date']; ?>
                                    </div>
                                </div>
                                <div class="row bg-light p-1 mt-1 text-center font-weight-bold">
                                    <div class="col-md-6 text-secondary">
                                        Election End date
                                    </div>
                                    <div class="col-md-6">
                                        <?php

                                        echo $data['election_end_date']; ?>
                                    </div>
                                </div>

                                <?php

                                if (!$eroObj->isResultGenerated($data['election_id'])) {
                                ?>
                                    <div class="row">
                                        <div class="col-12">
                                            <form action="" method="post">
                                                <div class="form-group">
                                                    <input type="hidden" name="candidate_id" value="<?php echo $data['candidate_id'] ?>">
                                                </div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <textarea id="my-textarea" class="form-control" style="resize:none;" name="comment" rows="3" placeholder="Comment"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-4">
                                                        <div class="btn-group btn-group-toggle text-center" data-toggle="buttons">
                                                            <label class="btn btn-outline-primary form-control ">
                                                                <input type="radio" name="status" value="1"> accept
                                                            </label>
                                                            <label class="btn btn-outline-danger">
                                                                <input type="radio" name="status" value="0"> Reject
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group mt-3">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <button class="btn btn-primary p-2 form-control" name="candiSubmit" value="true">Accept</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                <?php } else {
                                    # getting result of candidate 

                                } ?>

                            <?php
                            }
                            #ending else condition
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Main Content -->
        </div>

    </div>
    </div>



</body>

</html>

<?php

if (isset($_POST['candiSubmit'])) {
    $comment = $_POST['comment'];
    $candidate = $_POST['candidate_id'];
    $accept = $_POST['status'];

    // echo "$comment <br> $candidate <br> $voter <br> $election <br> accept = $accept <br>";

    if (isset($comment) && isset($candidate)  && isset($accept)) {
        $ero = new Ero();
        print_r($ero->candidateApplicationStatus($comment, $candidate, $accept));
    } else {

        echo '<script>alert("comment and candidare accept/reject status is mandatory !");</script>';
    }
}

?>