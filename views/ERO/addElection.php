<?php
require_once "../../Middleware/AuthEro.php";
$ero = $_SESSION['userEro'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../../assets/images/logo.png">
    <title>OVS | ERO:Dashboard</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../bootstrap/js/jquery.slim.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>

    <link rel="stylesheet" href="/OVS/views/ERO/dashboard.css">

</head>

<body>




    <!-- dashboard -->
    <div class="container-fluid">
        <div class="row">
            <!-- sidebar -->
            <div class="col-md-3 text-center mainSidebar">
                <div class="row">
                    <nav class="navbar bg-warning sticky-top col-12" style="padding:10px;width:100%">
                        <img src="../../assets/images/4.jpg" width="50" height="50" class="d-inline-block align-top ml-5" style="border:2px solid white;border-radius:10px;" alt="" loading="lazy">
                        <a class="navbar-brand ml-2 text-white" href="/OVS">Dashboard</a>
                        <button class="navbar-toggler" disabled type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/dashboard.php">Home</a>
                    </div>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/voterlist.php">Voter</a>
                    </div>
                    <div class="col-12 sidebarList sidebarActive">
                        <a href="/OVS/views/ERO/elections.php">Election</a>
                    </div>
                    <div class="col-12 sidebarList">
                        <a href="/OVS/views/ERO/electionresult.php"> Election Results</a>
                    </div>
                </div>
            </div>
            <!-- sidebar end -->
            <!--  -->
            <div class="col-md-9" style="padding:0px;margin:0px">

                <!-- Start Navigation -->
                <nav class="navbar navbar-expand-lg navbar-light bg-warning col-12" style="padding:16px;">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            Welcome &nbsp;
                            <strong>
                                <?php
                                // var_dump($_SESSION['userEro']);                 
                                echo $ero['username'];
                                ?> </strong>
                        </ul>
                        <div style="float:right">
                            <a href="../../Models/Logout.php?logout" class="btn btn-outline-info">Logout</a>

                        </div>
                    </div>
                </nav>

                <!-- End Navigation -->



                <!-- Main Content -->
                <div class="container-fluid mainContent" style="background-color: #f2f2f2;">
                    <?php
                    if (isset($_SESSION['error'])) {

                    ?>

                        <div class="container mt-3">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-6">
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <strong>
                                            <?php
                                            echo $_SESSION['error'];
                                            ?>
                                        </strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php
                        unset($_SESSION['error']);
                    }
                    ?>
                    <div class="row mt-2 mb-5">
                        <div class="col-md-2"></div>
                        <div class="col-md-6 card">
                            <div class="card-header text-center">
                                <h3>Add Election</h3>
                            </div>
                            <div class="card-body">
                                <form action="" method="post">
                                    <div class="form-group">
                                        <label for=""> Election Title</label>
                                        <input type="text" class="form-control" name="election_title" placeholder="Enter Election title">
                                    </div>
                                    <div class="form-group">
                                        <label for=""> Election Type</label>
                                        <input type="text" class="form-control" name="election_type" placeholder="Enter Election type">
                                    </div>
                                    <div class="form-group">
                                        <label for=""> Election Area PIN</label>
                                        <input type="text" class="form-control" name="election_area" placeholder="Enter Election area pin">
                                    </div>

                                    <div class="form-group">
                                        <label for=""> Select registration start date</label>
                                        <input type="date" class="form-control" name="reg_start_date">
                                    </div>
                                    <div class="form-group">
                                        <label for=""> Select registration end date</label>
                                        <input type="date" class="form-control" name="reg_end_date">
                                    </div>
                                    <div class="form-group">
                                        <label for=""> Select Election start date and time</label>
                                        <input type="datetime-local" class="form-control" name="election_start_date">
                                    </div>
                                    <div class="form-group">
                                        <label for=""> Select electiom end date and time</label>
                                        <input type="datetime-local" class="form-control" name="election_end_date">
                                    </div>

                            </div>
                            <div class="card-footer">
                                <button class="form-control btn btn-info" name="addElection">Add</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Main Content -->
            </div>

        </div>
    </div>

</body>

</html>

<?php
if (isset($_POST['addElection'])) {
    include_once '../../Models/Ero.php';
    $ero = new Ero();
    if (!empty($_POST['election_title']) && !empty($_POST['election_type']) && !empty($_POST['election_area']) && !empty($_POST['reg_start_date']) && !empty($_POST['reg_end_date']) && !empty($_POST['election_start_date']) && !empty($_POST['election_end_date'])) {
        $res =  $ero->addElection($_POST['election_title'], $_POST['election_type'], $_POST['election_area'], $_POST['reg_start_date'], $_POST['reg_end_date'], $_POST['election_start_date'], $_POST['election_end_date']);
    } else {
        $_SESSION['error'] = "All fields are require !";
    }
}
?>