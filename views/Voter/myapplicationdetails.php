<?php
include_once '../../Models/Auth.php';
include_once '../../Middleware/AuthVoter.php';
include '../../Models/Voter.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../../assets/images/logo.png">
    <title>Application details </title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../bootstrap/js/jquery.slim.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>
    <style>
        .profile-header {
            background: #ffe259;
            background: -webkit-linear-gradient(to right, #ffa751, #ffe259);
            background: linear-gradient(to right, #ffa751, #ffe259);
        }

        .navbar {
            background: #BBD2C5;
            /* fallback for old browsers */
            background: -webkit-linear-gradient(to right, #BBD2C5, #292E49, #536976, #BBD2C5);
            /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #BBD2C5, #292E49, #536976, #BBD2C5);
            /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

        }
    </style>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="../../assets/images/logo.png" alt="" width="80" height="80">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="/OVS/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/OVS/views/Voter/myapplications.php">My Applications</a>
                    </li>
                </ul>
            </div>
        </div>

        <a href="/OVS/views/Voter/profile.php" class="btn btn-info float-right m-2" style="float:right">Profile</a>
        <a href="../../Models/Logout.php?logout" class="btn btn-outline-info float-right" style="float:right">Logout</a>
    </nav>

    <?php
    if (isset($_SESSION['error'])) {
    ?>
        <div class="container">
            <div class="row error-alert">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>
                                <?php
                                echo $_SESSION['error'];
                                ?>
                            </strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (isset($_GET['success'])) {
            echo ('<script>
            setTimeout("location.href = `/OVS/Models/Logout.php?logout`;",2000);
            </script>');
        }
        ?>
    <?php
        unset($_SESSION['error']);
    }
    ?>
    <?php
    $data = Voter::getApplicationsForElection($_GET['applEle']);
    ?>
    <section class="bg-light p-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12 card p-0 m-0">
                    <div class="card-header">
                        <h3>My Details</h3>
                    </div>
                    <div class="card-body">
                        <?php
                        if ($data == null) {
                            echo '
                                    <div class="text-center text-secondary font-weight-bold">
                                        NO DATA FOUND
                                    </div>
                                ';
                        } else {
                            $count = 1;
                            foreach ($data as $ele) {

                        ?>

                                <div class="row p-3 <?php if (!$ele['candidate_election_status']) {
                                                        echo 'bg-warning';
                                                    } ?>">
                                    <div class="col"><?php echo $count; ?> </div>
                                    <div class="col"><?php echo $ele['election_title']; ?></div>
                                    <div class="col"><?php echo $ele['election_type']; ?></div>
                                    <div class="col"><?php echo $ele['election_area']; ?></div>
                                    <div class="col"><?php echo $ele['election_start']; ?></div>
                                    <div class="col"><?php echo $ele['comment']; ?></div>

                                </div>
                                <?php
                                //Started election done part
                                if ($ele['is_election_done']) {
                                ?>

                                    <div class="row p-3 m-0 bg-light border  border-success">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-3">
                                                    <strong>Votes</strong>
                                                </div>
                                                <div class="col-3">
                                                    <span class="p-2 bg-success text-white rounded-circle"><?php echo $ele['vote_count'] ?></span>
                                                </div>
                                                <div class="col-6 p-2 text-center font-weight-bold text-white <?php if ($ele['is_winner']) {
                                                                                                                    echo 'bg-success';
                                                                                                                } else {
                                                                                                                    echo 'bg-danger';
                                                                                                                } ?>">
                                                    <?php if ($ele['is_winner']) {
                                                        echo "Congratulations ! You are Won !";
                                                    } else {
                                                        echo "You loss !";
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                <?php
                                }
                                //end election done part
                                ?>

                        <?php
                                $count++;
                            }
                        } //else is end here for data
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</body>

</html>

<?php
if (isset($_POST['submitPass'])) {
    $voter = new Voter();
    $voter->changePassword($_POST['currentPass'], $_POST['newPass'], $_POST['confNewPass']);
}

?>