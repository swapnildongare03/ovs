<?php
include_once '../../Models/Auth.php';
include_once '../../Middleware/AuthVoter.php';
include '../../Models/Voter.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../../assets/images/logo.png">
    <title>Setting</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../bootstrap/js/jquery.slim.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>
    <style>
        .profile-header {
            background: #ffe259;
            background: -webkit-linear-gradient(to right, #ffa751, #ffe259);
            background: linear-gradient(to right, #ffa751, #ffe259);
        }

        .navbar {
            background: #BBD2C5;
            background: -webkit-linear-gradient(to right, #BBD2C5, #292E49, #536976, #BBD2C5);
            background: linear-gradient(to right, #BBD2C5, #292E49, #536976, #BBD2C5);
        }
    </style>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="../../assets/images/logo.png" alt="" width="80" height="80">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/OVS/">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/OVS/views/Voter/myapplications.php">My Applications</a>
                    </li>
                </ul>
            </div>
        </div>

        <a href="/OVS/views/Voter/profile.php" class="btn btn-info float-right m-2" style="float:right">Profile</a>
        <a href="../../Models/Logout.php?logout" class="btn btn-outline-info float-right" style="float:right">Logout</a>
    </nav>

    <?php
    if (isset($_SESSION['error'])) {
    ?>
        <div class="container">
            <div class="row error-alert">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>
                                <?php
                                echo $_SESSION['error'];
                                ?>
                            </strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (isset($_GET['success'])) {
            echo ('<script>
            setTimeout("location.href = `/OVS/Models/Logout.php?logout`;",2000);
            </script>');
        }
        ?>
    <?php
        unset($_SESSION['error']);
    }
    ?>

    <section class="bg-light p-3">
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8 card p-0 m-0">
                    <div class="card-header">
                        <h3>Change Password</h3>
                    </div>
                    <div class="card-body">
                        <form action="" method="post">
                            <div class="form-group" style="width:50%">
                            <label for="input"><small class="text-secondary"><small>if password is not reset </small>(yyyy-mm-dd)</small></label>
                                <input type="password" class="form-control" placeholder="Current password" name="currentPass">
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="new password" name="newPass">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="password" class="form-control" placeholder="confirm new password" name="confNewPass">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-success form-control" style="width:70%" name="submitPass">Change Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

</body>

</html>

<?php
if (isset($_POST['submitPass'])) {
    if ($_POST['currentPass'] != NULL && $_POST['newPass'] != NULL && $_POST['confNewPass'] != NULL) {
        $voter = new Voter();
        echo $voter->changePassword($_POST['currentPass'], $_POST['newPass'], $_POST['confNewPass']);
    } else {
        $_SESSION['error'] = "All fields are mandatory !";
    }
}

?>