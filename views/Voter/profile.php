<?php
include_once '../../Models/Auth.php';
include_once '../../Middleware/AuthVoter.php';
include '../../Models/Voter.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="../../assets/images/logo.png">
  <title>Profile</title>
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <script src="../../bootstrap/js/jquery.slim.min.js"></script>
  <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../../bootstrap/js/bootstrap.min.js"></script>
  <script src="../../bootstrap/js/popper.min.js"></script>
  <style>
    .profile-header {
      background: #ffe259;
      background: -webkit-linear-gradient(to right, #ffa751, #ffe259);
      background: linear-gradient(to right, #ffa751, #ffe259);
    }

    .navbar {
      background: #BBD2C5;
      /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #BBD2C5, #292E49, #536976, #BBD2C5);
      /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #BBD2C5, #292E49, #536976, #BBD2C5);
      /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }
  </style>
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">
        <img src="../../assets/images/logo.png" alt="" width="80" height="80">
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/OVS/">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/OVS/views/Voter/myapplications.php">My Applications</a>
          </li>
        </ul>
      </div>
    </div>

    <a href="/OVS/views/Voter/settings.php" class="btn btn-info float-right m-2" style="float:right">Setting</a>
    <a href="../../Models/Logout.php?logout" class="btn btn-outline-info float-right" style="float:right">Logout</a>
  </nav>

  <?php
  $auth = new Auth();
  ?>
  <section class="profile-header p-3">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-5 m-0 p-0   text-center  align-self-center">

          <?php
          if ($auth->user()['profile_url'] != NULL) {
            echo '<img src="' . $auth->user()['profile_url'] . '" alt="" class="img-fluid" width="50%" style="border-radius:100px;">';
          } else {
            echo '<img src="/OVS/assets/images/logo.png" alt="" class="img-fluid" width="50%">';
          }
          ?>

        </div>
        <div class="col-md-6 card">
          <div class="card-body row">
            <div class="col-md-6">Name</div>
            <div class="col-md-6"> <strong><?php echo $auth->user()['voter_name']; ?></strong></div>

            <div class="col-md-6 mt-4">Voter Id</div>
            <div class="col-md-6 mt-4"><?php echo $auth->user()['voter_id']; ?></div>

            <div class="col-md-6 mt-4">Date of Birth <small class="text-secondary"> yyyy-mm-dd</small></div>
            <div class="col-md-6 mt-4"><?php echo $auth->user()['dob']; ?></div>

            <div class="col-md-6 mt-4">Address</div>
            <div class="col-md-6 mt-4"><?php echo $auth->user()['address']; ?></div>

            <div class="col-md-6 mt-4">Gender</div>
            <div class="col-md-6 mt-4"><?php echo $auth->user()['gender']; ?></div>

            <div class="col-md-6 mt-4">Area</div>
            <div class="col-md-6 mt-4"><?php echo $auth->user()['area']; ?></div>
            <?php
            if ($auth->user()['password'] == null) {
            ?>
              <div class="col-12 text-center mt-3">
                <small class="text-danger">You have to change password first !</small>
                <a href="/OVS/views/Voter/settings.php" class="btn btn-warning">Change Password</a>
              </div>
            <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>


  <?php

  $voter = new Voter();
  $electionforRegister = $voter->getActiveElectionsForRegister();
  $electionForVote = $voter->getActiveElectionsForVote();
  ?>

  <section class="mt-5">
    <div class="container">
      <div class="row card">
        <div class="col-12 card-header text-secondary">
          <h3>Apply for election as a candidate </h3>
        </div>
        <div class="col-12 card-body">
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Sr. No</th>
                <th scope="col">Title</th>
                <th scope="col">Type</th>
                <th scope="col">Area</th>
                <th scope="col">Reg. start Date</th>
                <th scope="col">Reg. end Date</th>
                <th scope="col">Election Date</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <?php
            $count = 1;
            ?>
            <tbody id="voterTable">
              <?php
              if ($electionforRegister != null) {
                for ($i = 0; $i < count($electionforRegister); $i++) {
              ?>
                  <tr>
                    <th scope="row"><?php echo $count; ?></th>
                    <td><?php echo $electionforRegister[$i]['election_title']; ?></td>
                    <td><?php echo $electionforRegister[$i]['election_type']; ?></td>
                    <td><?php echo $electionforRegister[$i]['election_area']; ?></td>
                    <td><?php echo $electionforRegister[$i]['reg_start_date']; ?></td>
                    <td><?php echo $electionforRegister[$i]['reg_end_date']; ?></td>
                    <td><?php echo $electionforRegister[$i]['election_start_date']; ?></td>
                    <td>
                      <?php
                      $query = "SELECT * FROM candidate_election WHERE voter_id = " . Auth::user()['voter_id'] . " AND election_id = " . $electionforRegister[$i]['election_id'];
                      $canEle = Voter::select($query);
                      if ($canEle) {
                        echo '<span class="text-success">Successfully applied !</span>';
                      } else {
                      ?>
                        <a href="/OVS/views/Voter/applyforelection.php?elid=<?php echo $electionforRegister[$i]['election_id'] ?>" class="btn btn-warning">Apply for Candidate</a>
                      <?php } ?>
                    </td>
                  </tr>

                <?php
                  $count++;
                }
              } else {
                ?>
                <tr>
                  <th scope="row"></th>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td class="text-secondary">No data found</td>
                  <td></td>
                  <td>

                  </td>
                </tr>
              <?php
              }
              ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>

  <section class="mt-5 bg-light p-2">
    <div class="container">
      <div class="row card">
        <div class="col-12 card-header bg-white text-secondary">
          <h3>Elections for vote </h3>
        </div>
        <div class="col-12 card-body">
          <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Sr. No</th>
                <th scope="col">Title</th>
                <th scope="col">Type</th>
                <th scope="col">Area</th>
                <th scope="col">Election Start Date</th>
                <th scope="col">Election End Date</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <?php
            $count = 1;
            ?>
            <tbody id="voterTable">
              <?php
              if ($electionForVote != null) {
                for ($i = 0; $i < count($electionForVote); $i++) {
              ?>
                  <tr>
                    <th scope="row"><?php echo $count; ?></th>
                    <td><?php echo $electionForVote[$i]['election_title']; ?></td>
                    <td><?php echo $electionForVote[$i]['election_type']; ?></td>
                    <td><?php echo $electionForVote[$i]['election_area']; ?></td>
                    <td><?php echo $electionForVote[$i]['election_start_date']; ?></td>
                    <td><?php echo $electionForVote[$i]['election_end_date']; ?></td>
                    <td>
                      <?php
                      if (Voter::isValidVoter($electionForVote[$i]['election_id'], Auth::user()['voter_id'])) {
                      ?>
                        <a href="/OVS/views/Voter/vote.php?eleID=<?php echo $electionForVote[$i]['election_id']; ?>" class="btn btn-success">Vote</a>
                      <?php
                      } else {
                        echo '
                            <span class="text-danger">Voted !</span>
                          ';
                      }
                      ?>
                    </td>
                  </tr>

                <?php
                  $count++;
                }
              } else {


                ?>

                <tr>
                  <th scope="row"></th>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td class="text-secondary">No data found</td>
                  <td></td>
                  <td>

                  </td>
                </tr>
              <?php
              }
              ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>



</body>

</html>