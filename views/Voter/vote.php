<?php
include_once '../../Models/Auth.php';
include_once '../../Middleware/AuthVoter.php';
include '../../Middleware/VoterPasswordNull.php';
include '../../Models/Voter.php';
$voter = new Voter;
$auth = new Auth;
$isValidVoter = Voter::isValidVoter($_GET['eleID'], $auth->user()['voter_id']);
$isValidElection = Voter::isValidElection($_GET['eleID']);


if (!isset($_GET['eleID']) || $_GET['eleID'] == NULL || !$isValidVoter || !$isValidElection) {
    echo '
    
    <div style="padding : 100px;background-color:red;font-weight:bold;text-align:center;self-alignment:center;">
    you are un authorized
    <p style="color:white;">You area getting back in a while</p>
    <script>
        setTimeout(function(){
            window.location.href="/OVS/views/Voter/profile.php";
        },3000);
    </script>
    </div>    
    ';
} else {
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="../../assets/images/logo.png">
        <title>Vote for candidate</title>
        <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
        <script src="../../bootstrap/js/jquery.slim.min.js"></script>
        <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../../bootstrap/js/bootstrap.min.js"></script>
        <script src="../../bootstrap/js/popper.min.js"></script>
        <style>
            .profile-header {
                background: #ffe259;
                background: -webkit-linear-gradient(to right, #ffa751, #ffe259);
                background: linear-gradient(to right, #ffa751, #ffe259);
            }

            .navbar {
                background: #BBD2C5;
                background: -webkit-linear-gradient(to right, #BBD2C5, #292E49, #536976, #BBD2C5);
                background: linear-gradient(to right, #BBD2C5, #292E49, #536976, #BBD2C5);
            }
        </style>
    </head>

    <body>


        <?php
        if (isset($_SESSION['error'])) {
        ?>
            <div class="container">
                <div class="row error-alert">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>
                                    <?php
                                    echo $_SESSION['error'];
                                    ?>
                                </strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php
            unset($_SESSION['error']);
        }
        $data = Voter::getCandidateListForEletion($_GET['eleID']);
        ?>


        <section class="navbar text-white font-weight-bold">
            <h3>Candidate List</h3>
        </section>
        <div class="container-fluid">
            <div class="row ">

                <?php
                if (empty($data)) {
                    echo "<h1>No candidate found</h1>";
                } else {
                    foreach ($data as $voteData) {
                ?>

                        <div class="col-md-3 card p-0 m-4">
                            <div class="card-header text-center">
                                <img src="<?php echo $voteData['voter_profile']; ?>" alt="" class="img-fluid" width="50%">
                                <h6 class="mt-2"><?php echo $voteData['voter_name'] ?></h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6 text-fluid">name</div>
                                    <div class="col-6 text-fluid text-secondary">
                                        <?php echo $voteData['voter_name'] ?>
                                    </div>
                                    <div class="col-6 text-fluid">gender</div>
                                    <div class="col-6 text-fluid text-secondary">
                                        <?php echo $voteData['voter_gender'] ?>
                                    </div>
                                    <div class="col-6 text-fluid">area</div>
                                    <div class="col-6 text-fluid text-secondary">
                                        <?php echo $voteData['voter_area'] ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <form action="" method="POST">
                                    <input type="hidden" name="election" value="<?php echo $voteData['election_id']; ?>">
                                    <input type="hidden" name="candidate" value="<?php echo $voteData['candidate_id']; ?>">

                                    <button type="button" class="btn btn-success form-control" data-toggle="modal" data-target="#examp<?php echo $voteData['candidate_id']; ?>leModal">
                                        Vote
                                    </button>
                                    <div class="modal fade" id="examp<?php echo $voteData['candidate_id']; ?>leModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body  font-weight-bolder">
                                                    <h3><?php echo $voteData['voter_name']; ?></h3>
                                                    <h5 class="text-danger">confirm vote !</h5>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary" name="confirmVote">Confirm</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                <?php
                    }
                }
                ?>
            </div>
        </div>

    </body>

    </html>

<?php
} //unauthorized else is end here !


if (isset($_POST['confirmVote'])) {
    $voter->voteForElection($_POST['election'], $_POST['candidate']);
}
?>