<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../../assets/images/logo.png">
    <title>OVS | Voter:login</title>
    <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
    <script src="../../bootstrap/js/jquery.slim.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../bootstrap/js/popper.min.js"></script>
    <style>
        .login {
            padding: 29px;
            background-color: white;
            border: 1px solid #e6e6e6;
            border-radius: 10px;
            flex: block;
        }
    </style>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/OVS/">Home</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container" style="margin-top: 50px;">
        <?php
        session_start();
        if (!empty($_SESSION['error'])) {
        ?>
            <div class="row error-alert">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>
                                <?php
                                echo $_SESSION['error'];
                                ?>
                            </strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            session_destroy();
        }
        ?>

        <div class="container">
            <div class="row m-2">
                <div class="col-md-4"></div>
                <div class="col-md-4 text-center" style="font-size:40px">
                    <strong> Voter Login</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="login">
                        <div class="text-center">
                            <img class="img-fluid" src="../../assets/images/6.jpg" width="90px" alt="" style="border-radius: 100px">
                        </div>
                        <div>
                            <form action="" method="POST">
                                <div class="form-group">
                                    <label for="username">Voter Id</label>
                                    <input type="text" class="form-control" name="voter" id="username">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password <small class="text-secondary">(yyyy-mm-dd)</small></label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-warning form-control" name="voterSubmit">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>
<?php
include_once '../../Models/Auth.php';
if (isset($_POST['voterSubmit'])) {
    if ($_POST['voter'] != null && $_POST['password'] != null) {
        $voter = $_POST['voter'];
        $password = $_POST['password'];

        $auth = new Auth();
        $res = $auth->authVoter($voter, $password);
    } else {
        $_SESSION['error'] = 'All fields are mandatory!';
    }
}
?>